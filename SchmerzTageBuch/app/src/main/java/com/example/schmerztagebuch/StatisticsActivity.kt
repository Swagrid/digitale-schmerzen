package com.example.schmerztagebuch

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.schmerztagebuch.data.EntryLoadSave
import com.github.mikephil.charting.charts.CombinedChart
import com.github.mikephil.charting.charts.ScatterChart
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.ValueFormatter
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class StatisticsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_statistics)

        val combinedChart = findViewById<CombinedChart>(R.id.scatterChart)  // Achte darauf, das ID in deinem XML-Layout ebenfalls zu ändern
        val dateFormat = SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.GERMAN)
        val dataList = EntryLoadSave.getAllEntries(this)
        val selfPainEntries = ArrayList<Entry>()
        val aiPainEntries = ArrayList<Entry>()
        if (dataList != null) {
            for (dataPoint in dataList) {
                val combinedDateTime = "${dataPoint.date} ${dataPoint.time}"
                val dateObject = dateFormat.parse(combinedDateTime)
                val xValue = dateObject.time.toFloat()
                selfPainEntries.add(Entry(xValue, dataPoint.self_pain?.toFloat() ?: 0f))
                aiPainEntries.add(Entry(xValue, dataPoint.ai_pain?.toFloat() ?: 0f))
            }
        }

        val selfPainLineDataSet = LineDataSet(selfPainEntries, "Self Reviewed Pain")
        selfPainLineDataSet.color = Color.YELLOW
        selfPainLineDataSet.lineWidth = 2f
        selfPainLineDataSet.setDrawCircles(false)
        selfPainLineDataSet.setDrawValues(false)

        val aiPainLineDataSet = LineDataSet(aiPainEntries, "AI Reviewed Pain")
        aiPainLineDataSet.color = Color.GREEN  // Oder eine andere gewünschte Farbe
        aiPainLineDataSet.lineWidth = 2f
        aiPainLineDataSet.setDrawCircles(false)
        aiPainLineDataSet.setDrawValues(false)

        val combinedData = CombinedData()
        combinedData.setData(LineData(selfPainLineDataSet, aiPainLineDataSet))

        val legend = combinedChart.legend
        legend.verticalAlignment = Legend.LegendVerticalAlignment.TOP
        legend.horizontalAlignment = Legend.LegendHorizontalAlignment.CENTER

        combinedChart.data = combinedData

        // Y-Achse Begrenzen:
        combinedChart.axisLeft.axisMinimum = 0f
        combinedChart.axisLeft.axisMaximum = 10f
        combinedChart.axisRight.isEnabled = false

        val xAxis = combinedChart.xAxis
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.labelRotationAngle = -90f
        xAxis.valueFormatter = object : ValueFormatter() {
            private val dateTimeFormat = SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.GERMAN)
            override fun getAxisLabel(value: Float, axis: AxisBase?): String {
                return dateTimeFormat.format(Date(value.toLong()))
            }
        }

        combinedChart.invalidate()  // Daten im Chart aktualisieren
    }

    fun onBack(view: View) {
        finish()
    }
}
