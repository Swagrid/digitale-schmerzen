package com.example.schmerztagebuch

import android.Manifest
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.graphics.Color
import android.icu.text.SimpleDateFormat
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import android.widget.SeekBar
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.schmerztagebuch.data.Entry
import com.example.schmerztagebuch.data.PainLocalized
import java.util.Calendar
import java.util.Locale

class EntryActivity : AppCompatActivity() {
    private lateinit var editTextDate: EditText
    private lateinit var editTextTime: EditText
    private lateinit var ownPainLevelTextView: TextView
    private lateinit var seekBar: SeekBar
    private val calendar = Calendar.getInstance()

    private val entry = Entry()

    var cam_uri: Uri? = null
    var startCamera: ActivityResultLauncher<Intent>? = null
    var img_uri: Uri? = null
    var askPermissions: ActivityResultLauncher<Array<String>>? = null

    private var lastSelectedButton: Button? = null
    private var selectedBodyPart: PainLocalized? = PainLocalized.noPain



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_entry)

        val button: ImageButton = findViewById(R.id.takePictureButton)
        if (startCamera == null) {
            startCamera = registerForActivityResult(
                ActivityResultContracts.StartActivityForResult()
            ) { result ->
                if (result.resultCode == RESULT_OK) {
                    var uri: Uri? = null
                    if (cam_uri != null)
                        uri = cam_uri
                    else if (img_uri != null)
                        uri = img_uri

                    val cursor = contentResolver.query(
                        uri!!,
                        (arrayOf(MediaStore.Images.Media.DATA)),
                        null,
                        null,
                        null
                    )

                    if (cursor != null) {
                        val column_index =
                            cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
                        cursor.moveToFirst()
                        val path = cursor.getString(column_index)
                        entry.pathImg = path
                        cursor.close()

                        val receivedPain =  HTTPRequestHandler.TransmitPicture(
                            path,
                            "test",
                            this
                        )
                        if (receivedPain < 0) {
                            findViewById<TextView>(R.id.aiPainLevelTextView).text =
                                "No Reference Pictures provided"
                            entry.ai_pain = -1.0
                        } else if (receivedPain > 100) {
                            findViewById<TextView>(R.id.aiPainLevelTextView).text =
                                "No Face detected"
                            entry.ai_pain = -1.0
                        } else {
                            findViewById<TextView>(R.id.aiPainLevelTextView).text =
                                receivedPain.toString()
                            entry.ai_pain = receivedPain
                        }

                        val bitmap = BitmapFactory.decodeFile(path)
                        button.setImageBitmap(bitmap)
                    }

                }
            }
        }

        askPermissions =
            registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { result ->
                if (result.containsKey(Manifest.permission.CAMERA) && result[Manifest.permission.CAMERA]!!) {
                    pickCamera()
                } else if (result.containsKey(Manifest.permission.READ_EXTERNAL_STORAGE) && result[Manifest.permission.READ_EXTERNAL_STORAGE]!!) {
                    openGallery()
                }
            }

        editTextDate = findViewById(R.id.editTextDate)
        editTextTime = findViewById(R.id.editTextTime)

        seekBar = findViewById(R.id.seekBar)
        ownPainLevelTextView = findViewById(R.id.ownPainLevelTextView)


        updateDateInView()
        updateTimeInView()

        seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                // Update the TextView with the current value
                ownPainLevelTextView.text = progress.toString()

                // Change the SeekBar color based on progress
                val color = getColorForValue(progress)
                ownPainLevelTextView.setTextColor(color)
                //seekBar?.progressDrawable?.setTint(color)
                seekBar?.thumb?.setTint(color)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
                // Not needed, but you can add custom logic if required
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                // Not needed, but you can add custom logic if required
            }
        })

        editTextDate.setOnClickListener {
            val datePickerDialog = DatePickerDialog(
                this@EntryActivity,
                DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                    calendar.set(Calendar.YEAR, year)
                    calendar.set(Calendar.MONTH, monthOfYear)
                    calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                    updateDateInView()
                },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
            )

            // Show the date picker dialog
            datePickerDialog.show()
        }

        editTextTime.setOnClickListener {
            val timePickerDialog = TimePickerDialog(
                this@EntryActivity,
                TimePickerDialog.OnTimeSetListener { _, hourOfDay, minute ->
                    calendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
                    calendar.set(Calendar.MINUTE, minute)
                    updateTimeInView()
                },
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                true // Set to true if you want 24-hour format; false for AM/PM format
            )

            // Show the time picker dialog
            timePickerDialog.show()
        }
        val toggleButton = findViewById<Button>(R.id.toggleButton)
        val bodyView = findViewById<View>(R.id.bodyView)

        toggleButton.setOnClickListener {
            if (bodyView.visibility == View.GONE) {
                bodyView.visibility = View.VISIBLE
                toggleButton.setText(getResources().getString(R.string.hide))
            } else {
                bodyView.visibility = View.GONE
                toggleButton.setText(getResources().getString(R.string.expand))
            }
        }

        val buttons = listOf(
            findViewById<Button>(R.id.headButton),
            findViewById<Button>(R.id.neckButton),
            findViewById<Button>(R.id.chestButton),
            findViewById<Button>(R.id.stomachButton),
            findViewById<Button>(R.id.rightArmButton),
            findViewById<Button>(R.id.leftArmButton),
            findViewById<Button>(R.id.rightLegButton),
            findViewById<Button>(R.id.leftLegButton),
            findViewById<Button>(R.id.rightFootButton),
            findViewById<Button>(R.id.leftFootButton)
        )

        buttons.forEach { button ->
            button.setBackgroundColor(ContextCompat.getColor(this, R.color.inactive_gray))
            button.setOnClickListener {
                onBodyPartClicked(button)
            }
        }
    }

    private fun openGallery() {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "New Picture")
        values.put(MediaStore.Images.Media.DESCRIPTION, "From Files")

        img_uri = contentResolver.insert(
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
            values
        )
        val filesIntent = Intent().apply {
            type = "image/*"
            action = Intent.ACTION_GET_CONTENT
        }
        filesIntent.putExtra(MediaStore.EXTRA_OUTPUT, img_uri)
        startCamera?.launch(filesIntent)
    }


    fun pickCamera() {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "New Picture")
        values.put(MediaStore.Images.Media.DESCRIPTION, "From Camera")

        cam_uri = contentResolver.insert(
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
            values
        )!!
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, cam_uri)

        startCamera?.launch(cameraIntent)
        startCamera.apply { }
    }

    fun onImage(view: View) {
        AlertDialog.Builder(this).setTitle("Camera or Files")
            .setMessage("Take a picture or browse files?")
            .setPositiveButton("camera") { _, _ ->
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED
                ) {
                    askPermissions?.launch(Array<String>(1) { Manifest.permission.CAMERA })
                } else {
                    pickCamera()
                }
            }.setNegativeButton("files") { _, _ ->
                if (ContextCompat.checkSelfPermission(
                        this,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    )
                    != PackageManager.PERMISSION_GRANTED
                ) {
                    askPermissions?.launch(Array<String>(1) { Manifest.permission.READ_EXTERNAL_STORAGE })
                } else {
                    openGallery()
                }
            }.setNeutralButton("cancel") { _, _ ->

            }.create().show()
    }

    fun onSubmit(view: View) {
        entry.date = editTextDate.text.toString()
        entry.time = editTextTime.text.toString()
        entry.self_pain = seekBar.progress
        entry.painLocalized = selectedBodyPart
        entry.note = findViewById<EditText>(R.id.noteEditText).text.toString()
        entry.counter_action =
            findViewById<EditText>(R.id.counterMeasurementsEditText).text.toString()
        entry.trigger = findViewById<EditText>(R.id.triggersEditText).text.toString()

        Toast.makeText(applicationContext, "Saving...", Toast.LENGTH_SHORT).show()
        val sharedPref = getSharedPreferences("data", MODE_PRIVATE) ?: return
        val len = sharedPref.getInt("size", 0)
        entry.id = len
        with(sharedPref.edit()) {
            putInt("size", len + 1)
            putString(len.toString(), entry.toJson())
            apply()
        }
        finish()
    }

    fun onCancel(view: View) {
        finish()
    }

    private fun updateDateInView() {
        val myFormat = "dd.MM.yyyy" // Choose your desired date format
        val sdf = SimpleDateFormat(myFormat, Locale.getDefault())
        editTextDate.setText(sdf.format(calendar.time))
    }

    private fun updateTimeInView() {
        val myFormat = "HH:mm" // Choose your desired time format
        val sdf = SimpleDateFormat(myFormat, Locale.getDefault())
        editTextTime.setText(sdf.format(calendar.time))
    }

    private fun getColorForValue(value: Int): Int {
        // Calculate a color based on the value (e.g., green to red gradient)
        val green = (255 * (10 - value) / 10).toInt()
        val red = (255 * value / 10).toInt()
        return Color.rgb(red, green, 0)
    }

    fun onBodyPartClicked(view: View) {
        if (view is Button) {
            lastSelectedButton?.setBackgroundColor(ContextCompat.getColor(this, R.color.inactive_gray))
            view.setBackgroundColor(ContextCompat.getColor(this, R.color.active_red))


            when (view.id) {
                R.id.headButton -> selectedBodyPart = PainLocalized.head
                R.id.neckButton -> selectedBodyPart = PainLocalized.neck
                R.id.chestButton -> selectedBodyPart = PainLocalized.chest
                R.id.stomachButton -> selectedBodyPart = PainLocalized.stomach
                R.id.leftArmButton -> selectedBodyPart = PainLocalized.left_arm
                R.id.rightArmButton -> selectedBodyPart = PainLocalized.right_arm
                R.id.leftLegButton -> selectedBodyPart = PainLocalized.left_leg
                R.id.rightLegButton -> selectedBodyPart = PainLocalized.right_leg
                R.id.leftFootButton -> selectedBodyPart = PainLocalized.left_foot
                R.id.rightFootButton -> selectedBodyPart = PainLocalized.left_foot
            }

            lastSelectedButton = view
        }
    }

}