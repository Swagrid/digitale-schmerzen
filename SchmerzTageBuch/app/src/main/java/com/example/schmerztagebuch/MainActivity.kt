package com.example.schmerztagebuch

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.schmerztagebuch.data.Entry
import com.google.gson.Gson
import android.Manifest
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts


class MainActivity : AppCompatActivity() {
    private val REQUEST_PERMISSION = 1000
    private val PICK_IMAGE = 1
    private lateinit var imagePickerLauncher: ActivityResultLauncher<String>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun onPressSettingsButton(view: View){
        val intent = Intent(this, SettingsActivity::class.java)
        startActivity(intent);
    }

    fun onPressNewEntryButton(view: View){
        val intent = Intent(this, EntryActivity::class.java)
        startActivity(intent);
    }

    fun onPressHistoryButton(view: View){
        val intent = Intent(this, HistoryActivity::class.java)
        startActivity(intent);
    }

    fun onPressStatisticsButton(view: View){
        val intent = Intent(this, StatisticsActivity::class.java)
        startActivity(intent)
    }
}