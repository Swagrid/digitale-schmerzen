package com.example.schmerztagebuch.ui.settings

enum class ImageValidation {
    notProved,
    invalid,
    valid
}