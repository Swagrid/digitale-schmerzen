package com.example.schmerztagebuch.data

enum class PainLocalized {
    noPain,
    head,
    neck,
    chest,
    stomach,
    left_arm,
    right_arm,
    left_leg,
    right_leg,
    left_foot,
    right_foot
}