import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.schmerztagebuch.EditEntryActivity
import com.example.schmerztagebuch.R
import com.example.schmerztagebuch.data.Entry
import android.content.SharedPreferences

class HistoryAdapter(private var entryList: MutableList<Entry>) : RecyclerView.Adapter<HistoryAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textDate: TextView = itemView.findViewById(R.id.textDate)
        val textTime: TextView = itemView.findViewById(R.id.textTime)
        val textOwnPain: TextView = itemView.findViewById(R.id.textOwnPain)
        //val textAIPain: TextView = itemView.findViewById(R.id.textAIPain)
        //val textLocalisation: TextView = itemView.findViewById(R.id.textLocalisation)
        val idTextView: TextView = itemView.findViewById(R.id.idTextView)
        val imageButton: ImageButton = itemView.findViewById(R.id.imageButton)

        init {
            // Füge den OnClickListener für den ImageButton hier in der ViewHolder-Initialisierung hinzu
            imageButton.setOnClickListener {
                // Hole die Position des angeklickten Elements
                val position = adapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val clickedEntry = entryList[position]

                    // Hier kannst du die gewünschte Aktion ausführen, z.B. eine Bearbeitungsaktivität starten
                    val context = itemView.context
                    val intent = Intent(context, EditEntryActivity::class.java)
                    intent.putExtra("id", clickedEntry.id) // Übergebe die ID des ausgewählten Eintrags, falls erforderlich
                    context.startActivity(intent)


                    /*val sharedPref = getSharedPreferences("data", AppCompatActivity.MODE_PRIVATE) ?: return
                    startActivity(Intent(this, EditEntryActivity::class.java).apply {
                        putExtra("id", 0)
                    }).apply {  }*/

                }
            }
        }

        fun bind(entry: Entry) {
            // Hier setzen Sie die Werte für die TextViews und ImageButton basierend auf dem Entry-Objekt
            textDate.text = "${entry.date}"
            textTime.text = "${entry.time}"
            textOwnPain.text = "${entry.self_pain}"
            idTextView.text = "${entry.id}"
            //textAIPain.text = "${entry.ai_pain}"
            //textLocalisation.text = "${entry.painLocalized}"
        }


    }

    fun updateData(newData: MutableList<Entry>) {
        entryList = newData
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.history_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(entryList[position])
    }

    override fun getItemCount(): Int {
        return entryList.size
    }

}
