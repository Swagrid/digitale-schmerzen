package com.example.schmerztagebuch.data

import com.google.gson.Gson
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.Date

class Entry() {

    var id: Int?= null
    var date: String? = null
    var time: String? = null
    var self_pain: Int? = null
    var ai_pain: Double? = null
    var painLocalized: PainLocalized? = null
    var note: String? = null
    var counter_action: String? = null
    var trigger: String? = null
    var pathImg: String? = null

    fun toJson(): String? {
        val gson = Gson()
        return gson.toJson(this)
    }

    fun fromJson(str: String) {

    }

    constructor(s: Int) : this() {
        id = s
    }

}