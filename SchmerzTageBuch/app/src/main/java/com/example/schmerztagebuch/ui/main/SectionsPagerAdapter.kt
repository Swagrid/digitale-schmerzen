package com.example.schmerztagebuch.ui.main

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.schmerztagebuch.R
import com.example.schmerztagebuch.SettingsFragment
import com.example.schmerztagebuch.SetupFragment

private val TAB_TITLES = arrayOf(
    R.string.settings,
    R.string.setup,
    R.string.notifications
)

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
class SectionsPagerAdapter(private val context: Context, fm: FragmentManager) :
    FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment.
        return when (position) {
            0 -> SettingsFragment() // Your existing tab fragment
            1 -> SetupFragment()  // Your newly added fragment
            //2 -> NotificationFragment()
            else -> throw IllegalArgumentException("Invalid position")
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return context.resources.getString(TAB_TITLES[position])
    }

    override fun getCount(): Int {
        // Show 2 total pages.
        return 2
    }
}