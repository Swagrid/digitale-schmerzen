import os
import signal
import sys
from flask import Flask, jsonify, request
import imageio
from pain_detector import PainDetector
import cv2



pain_detector = PainDetector(image_size=160, checkpoint_path='checkpoints/50342566/50343918_3/model_epoch4.pt', num_outputs=40)

if not os.path.exists("reference_pictures"):
    os.mkdir("reference_pictures")

for file in os.listdir("reference_pictures"):
    filename = os.fsdecode(file)
    try:
        pain_detector.add_reference(cv2.imread(f"reference_pictures/{filename}"))
    except ValueError:
        print(f"'reference_pictures/{filename}' cannot be used as reference picture!")

app = Flask(__name__)


@app.route("/check")
def hello_world():
    return ""

@app.route('/upload', methods=['POST'])
def upload():
    if len(os.listdir("reference_pictures")) == 0:
        return jsonify({"pain":str(-1.0)})
    file = request.files['test']
    #filename = secure_filename(file.filename)
    file.save("test.jpg")
    target_frame = cv2.imread("test.jpg")
    try:
        pain_estimate = pain_detector.predict_pain(target_frame)
    except ValueError as e:
        pain_estimate = 1000
    return jsonify({"pain":str(pain_estimate)})

@app.route('/reference', methods=['POST'])
def reference():
    ret = {"not_valid":[]}
    size = int(request.form.get("size"))
    if size > 0:
        for f in os.listdir("reference_pictures"):
            os.remove(os.path.join("reference_pictures", f))
        pain_detector.ref_frames = []
    for i in range(0,size):
        file = request.files[f'ref{i}']
        try:
            file.save(f"reference_pictures/ref{i}.jpg") 
            pain_detector.add_reference(cv2.imread(f"reference_pictures/ref{i}.jpg"))
        except ValueError:
            print(f"reference_pictures/ref{i}.jpg cannot be used as reference picture!")
            os.remove(f"reference_pictures/ref{i}.jpg")
            ret["not_valid"].append(i)
    return jsonify(ret)

if __name__ == '__main__':
    print("go")
    def signal_handler(sig, frame):
        print("Removing Images...")
        try:
            os.remove(os.path.join("test.jpg"))
        except:
            pass
        print("Done, farewell!")
        sys.exit(0)

    signal.signal(signal.SIGINT, signal_handler)
    app.run(host='0.0.0.0', port=5000)
    signal.pause()
    
    