package com.example.schmerztagebuch.data

class ResponseData() {
    var pain:Double = 0.0
    var not_valid: MutableList<Int>? = mutableListOf()
}