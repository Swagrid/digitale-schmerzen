package com.example.schmerztagebuch

import android.Manifest
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.graphics.Color
import android.icu.text.SimpleDateFormat
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import android.widget.SeekBar
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.net.toUri
import com.example.schmerztagebuch.data.Entry
import com.example.schmerztagebuch.data.PainLocalized
import com.google.gson.Gson
import java.util.Calendar
import java.util.Locale

class EditEntryActivity : AppCompatActivity() {

    private lateinit var editTextDate: EditText
    private lateinit var editTextTime: EditText
    private lateinit var ownPainLevelTextView: TextView
    private lateinit var seekBar: SeekBar
    private lateinit var ai_pain_view: TextView
    private lateinit var takePictureButton: ImageButton
    private lateinit var counterMeasurementsEditText: EditText
    private lateinit var triggersEditText: EditText
    private lateinit var noteEditText: EditText
    private lateinit var editButton: Button
    private lateinit var buttonSubmit: Button
    private lateinit var buttons:List<Button>

    private var lastSelectedButton: Button? = null
    private var selectedBodyPart: PainLocalized? = PainLocalized.noPain

    private val calendar = Calendar.getInstance()

    private var entry = Entry()

    var cam_uri: Uri? = null
    var startCamera: ActivityResultLauncher<Intent>? = null
    var img_uri: Uri? = null
    var askPermissions: ActivityResultLauncher<Array<String>>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_entry)
        takePictureButton = findViewById(R.id.takePictureButton)
        editTextDate = findViewById(R.id.editTextDate)
        editTextTime = findViewById(R.id.editTextTime)
        seekBar = findViewById(R.id.seekBar)
        ownPainLevelTextView = findViewById(R.id.ownPainLevelTextView)
        ai_pain_view = findViewById(R.id.aiPainLevelTextView)
        counterMeasurementsEditText = findViewById(R.id.counterMeasurementsEditText)
        triggersEditText = findViewById(R.id.triggersEditText)
        noteEditText = findViewById(R.id.noteEditText)
        editButton = findViewById(R.id.editButton)
        buttonSubmit = findViewById(R.id.buttonSubmit)

        buttons = listOf(
            findViewById<Button>(R.id.headButton),
            findViewById<Button>(R.id.neckButton),
            findViewById<Button>(R.id.chestButton),
            findViewById<Button>(R.id.stomachButton),
            findViewById<Button>(R.id.rightArmButton),
            findViewById<Button>(R.id.leftArmButton),
            findViewById<Button>(R.id.rightLegButton),
            findViewById<Button>(R.id.leftLegButton),
            findViewById<Button>(R.id.rightFootButton),
            findViewById<Button>(R.id.leftFootButton)
        )

        buttons.forEach { button ->
            button.isEnabled = false
        }



        val gson = Gson()
        val id = intent.getIntExtra("id", 0)

        val sharedPref = getSharedPreferences("data", MODE_PRIVATE) ?: return
        val string = sharedPref.getString("$id", "fail")

        if(string != "fail") {
            entry = gson.fromJson(string, Entry::class.java)
        } else {
            Toast.makeText(this, "NO DATA", Toast.LENGTH_SHORT).show()
            this.finish()
        }

        takePictureButton.isEnabled = false
        if (entry.pathImg != null){
            takePictureButton.setImageURI(entry.pathImg!!.toUri())
        }
        editTextDate.isEnabled = false
        editTextDate.setText(entry.date)
        editTextTime.isEnabled = false
        editTextTime.setText(entry.time)
        seekBar.isEnabled = false
        if(entry.self_pain != null)
            seekBar.progress = entry.self_pain!!
        counterMeasurementsEditText.isEnabled = false
        counterMeasurementsEditText.setText(entry.counter_action)
        triggersEditText.isEnabled = false
        triggersEditText.setText(entry.trigger)
        noteEditText.isEnabled = false
        noteEditText.setText(entry.note)

        ownPainLevelTextView.setText(entry.self_pain.toString())
        ai_pain_view.setText(entry.ai_pain.toString())


        buttons.forEach { button ->
            button.setBackgroundColor(ContextCompat.getColor(this, R.color.inactive_gray))
            button.setOnClickListener {
                onBodyPartClicked(button)
            }
        }
        entry.painLocalized?.let { setupPainLocalization(it) }


        val button: ImageButton = findViewById(R.id.takePictureButton)
        if (startCamera == null) {
            startCamera = registerForActivityResult(
                ActivityResultContracts.StartActivityForResult()
            ) { result ->
                if (result.resultCode == RESULT_OK) {
                    var uri: Uri? = null
                    if (cam_uri != null)
                        uri = cam_uri
                    else if (img_uri != null)
                        uri = img_uri

                    val cursor = contentResolver.query(
                        uri!!,
                        (arrayOf(MediaStore.Images.Media.DATA)),
                        null,
                        null,
                        null
                    )

                    if (cursor != null) {
                        val column_index =
                            cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
                        cursor.moveToFirst()
                        val path = cursor.getString(column_index)
                        entry.pathImg = path
                        cursor.close()

                        val receivedPain = HTTPRequestHandler.TransmitPicture(
                            path,
                            "test",
                            this
                        )
                        if (receivedPain < 0) {
                            findViewById<TextView>(R.id.aiPainLevelTextView).text =
                                "No Reference Pictures provided"
                            entry.ai_pain = -1.0
                        } else if (receivedPain > 100) {
                            findViewById<TextView>(R.id.aiPainLevelTextView).text =
                                "No Face detected"
                            entry.ai_pain = -1.0
                        } else {
                            findViewById<TextView>(R.id.aiPainLevelTextView).text =
                                receivedPain.toString()
                            entry.ai_pain = receivedPain
                        }

                        val bitmap = BitmapFactory.decodeFile(path)
                        button.setImageBitmap(bitmap)
                    }

                }
            }

        }


        seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                // Update the TextView with the current value
                ownPainLevelTextView.text = progress.toString()

                // Change the SeekBar color based on progress
                val color = getColorForValue(progress)
                ownPainLevelTextView.setTextColor(color)
                //seekBar?.progressDrawable?.setTint(color)
                seekBar?.thumb?.setTint(color)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
                // Not needed, but you can add custom logic if required
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                // Not needed, but you can add custom logic if required
            }
        })

        editTextDate.setOnClickListener {
            val datePickerDialog = DatePickerDialog(
                this@EditEntryActivity,
                DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                    calendar.set(Calendar.YEAR, year)
                    calendar.set(Calendar.MONTH, monthOfYear)
                    calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                    updateDateInView()
                },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
            )

            // Show the date picker dialog
            datePickerDialog.show()
        }

        editTextTime.setOnClickListener {
            val timePickerDialog = TimePickerDialog(
                this@EditEntryActivity,
                TimePickerDialog.OnTimeSetListener { _, hourOfDay, minute ->
                    calendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
                    calendar.set(Calendar.MINUTE, minute)
                    updateTimeInView()
                },
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                true // Set to true if you want 24-hour format; false for AM/PM format
            )

            // Show the time picker dialog
            timePickerDialog.show()
        }

        val toggleButton = findViewById<Button>(R.id.toggleButton2)
        val bodyView = findViewById<View>(R.id.bodyView)

        toggleButton.setOnClickListener {
            if (bodyView.visibility == View.GONE) {
                bodyView.visibility = View.VISIBLE
                toggleButton.setText(getResources().getString(R.string.hide))
            } else {
                bodyView.visibility = View.GONE
                toggleButton.setText(getResources().getString(R.string.expand))
            }
        }


    }

    fun onEdit(view: View){
        takePictureButton.isEnabled = true
        editTextDate.isEnabled = true
        editTextTime.isEnabled = true
        seekBar.isEnabled = true
        counterMeasurementsEditText.isEnabled = true
        triggersEditText.isEnabled = true
        noteEditText.isEnabled = true
        buttonSubmit.isEnabled = true
        editButton.isEnabled = false
        buttons.forEach { button ->
            button.isEnabled = true
        }
    }

    private fun openGallery() {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "New Picture")
        values.put(MediaStore.Images.Media.DESCRIPTION, "From Files")

        img_uri = contentResolver.insert(
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
            values
        )
        val filesIntent = Intent().apply {
            type = "image/*"
            action = Intent.ACTION_GET_CONTENT
        }
        filesIntent.putExtra(MediaStore.EXTRA_OUTPUT, img_uri)
        startCamera?.launch(filesIntent)
    }


    fun pickCamera() {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "New Picture")
        values.put(MediaStore.Images.Media.DESCRIPTION, "From Camera")

        cam_uri = contentResolver.insert(
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
            values
        )!!
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, cam_uri)

        startCamera?.launch(cameraIntent)
        startCamera.apply { }
    }

    fun onImage(view: View) {
        AlertDialog.Builder(this).setTitle("Camera or Files")
            .setMessage("Take a picture or browse files?")
            .setPositiveButton("camera") { _, _ ->
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED
                ) {
                    askPermissions?.launch(Array<String>(1) { Manifest.permission.CAMERA })
                } else {
                    pickCamera()
                }
            }.setNegativeButton("files") { _, _ ->
                if (ContextCompat.checkSelfPermission(
                        this,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    )
                    != PackageManager.PERMISSION_GRANTED
                ) {
                    askPermissions?.launch(Array<String>(1) { Manifest.permission.READ_EXTERNAL_STORAGE })
                } else {
                    openGallery()
                }
            }.setNeutralButton("cancel") { _, _ ->

            }.create().show()
    }

    fun onSubmit(view: View) {
        entry.date = editTextDate.text.toString()
        entry.time = editTextTime.text.toString()
        entry.self_pain = seekBar.progress
        entry.painLocalized = selectedBodyPart
        entry.note = findViewById<EditText>(R.id.noteEditText).text.toString()
        entry.counter_action =
            findViewById<EditText>(R.id.counterMeasurementsEditText).text.toString()
        entry.trigger = findViewById<EditText>(R.id.triggersEditText).text.toString()

        Toast.makeText(applicationContext, "Saving...", Toast.LENGTH_SHORT).show()
        val sharedPref = getSharedPreferences("data", MODE_PRIVATE) ?: return
        with(sharedPref.edit()) {
            putString(entry.id.toString(), entry.toJson())
            apply()
        }
        finish()
    }

    fun onCancel(view: View) {
        finish()
    }

    private fun setupPainLocalization(painLocalized: PainLocalized){
        when (painLocalized) {
            PainLocalized.head -> lastSelectedButton = findViewById<Button>(R.id.headButton)
            PainLocalized.neck -> lastSelectedButton = findViewById<Button>(R.id.neckButton)
            PainLocalized.chest -> lastSelectedButton = findViewById<Button>(R.id.chestButton)
            PainLocalized.stomach -> lastSelectedButton = findViewById<Button>(R.id.stomachButton)
            PainLocalized.left_arm -> lastSelectedButton = findViewById<Button>(R.id.leftArmButton)
            PainLocalized.right_arm -> lastSelectedButton = findViewById<Button>(R.id.rightArmButton)
            PainLocalized.left_leg -> lastSelectedButton = findViewById<Button>(R.id.leftLegButton)
            PainLocalized.right_leg -> lastSelectedButton = findViewById<Button>(R.id.rightLegButton)
            PainLocalized.left_foot -> lastSelectedButton = findViewById<Button>(R.id.leftFootButton)
            PainLocalized.right_foot -> lastSelectedButton = findViewById<Button>(R.id.rightFootButton)

            else -> {selectedBodyPart = PainLocalized.noPain}
        }
        selectedBodyPart = painLocalized
        if (painLocalized != PainLocalized.noPain){
            lastSelectedButton?.setBackgroundColor(ContextCompat.getColor(this, R.color.active_red))
        }
    }

    private fun updateDateInView() {
        val myFormat = "dd.MM.yyyy" // Choose your desired date format
        val sdf = SimpleDateFormat(myFormat, Locale.getDefault())
        editTextDate.setText(sdf.format(calendar.time))
    }

    private fun updateTimeInView() {
        val myFormat = "HH:mm" // Choose your desired time format
        val sdf = SimpleDateFormat(myFormat, Locale.getDefault())
        editTextTime.setText(sdf.format(calendar.time))
    }

    private fun getColorForValue(value: Int): Int {
        // Calculate a color based on the value (e.g., green to red gradient)
        val green = (255 * (10 - value) / 10).toInt()
        val red = (255 * value / 10).toInt()
        return Color.rgb(red, green, 0)
    }

    fun onBodyPartClicked(view: View) {
        if (view is Button) {
            lastSelectedButton?.setBackgroundColor(ContextCompat.getColor(this, R.color.inactive_gray))
            view.setBackgroundColor(ContextCompat.getColor(this, R.color.active_red))


            when (view.id) {
                R.id.headButton -> selectedBodyPart = PainLocalized.head
                R.id.neckButton -> selectedBodyPart = PainLocalized.neck
                R.id.chestButton -> selectedBodyPart = PainLocalized.chest
                R.id.stomachButton -> selectedBodyPart = PainLocalized.stomach
                R.id.leftArmButton -> selectedBodyPart = PainLocalized.left_arm
                R.id.rightArmButton -> selectedBodyPart = PainLocalized.right_arm
                R.id.leftLegButton -> selectedBodyPart = PainLocalized.left_leg
                R.id.rightLegButton -> selectedBodyPart = PainLocalized.right_leg
                R.id.leftFootButton -> selectedBodyPart = PainLocalized.left_foot
                R.id.rightFootButton -> selectedBodyPart = PainLocalized.right_foot
            }

            lastSelectedButton = view
        }
    }

}