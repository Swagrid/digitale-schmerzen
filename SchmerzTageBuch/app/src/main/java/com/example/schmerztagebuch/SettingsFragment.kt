package com.example.schmerztagebuch

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.InetAddresses
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Switch
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.app.AppCompatDelegate.MODE_NIGHT_NO
import androidx.appcompat.app.AppCompatDelegate.MODE_NIGHT_YES
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import com.example.schmerztagebuch.data.EntryLoadSave
import java.io.File

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [SettingsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SettingsFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    @SuppressLint("UseSwitchCompatOrMaterialCode")
    private lateinit var darkModeSwitch: Switch

    @SuppressLint("UseSwitchCompatOrMaterialCode")
    private lateinit var notificationsSwitch: Switch


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onStop() {
        super.onStop()
        val ip = requireActivity().findViewById<EditText>(R.id.editIPText).text.toString()
        val darkMode = requireActivity().findViewById<Switch>(R.id.darkModeSwitch).isChecked
        val notification =
            requireActivity().findViewById<Switch>(R.id.notificationsSwitch).isChecked
        with(requireActivity().getPreferences(AppCompatActivity.MODE_PRIVATE).edit()) {
            putString("IP", ip)
            putBoolean("DM", darkMode)
            putBoolean("NO", notification)
            apply()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_settings_2, container, false)
        val editText = rootView.findViewById<EditText>(R.id.exportEditText)

        val ip =
            requireActivity().getPreferences(AppCompatActivity.MODE_PRIVATE).getString("IP", "")
        val darkMode =
            requireActivity().getPreferences(AppCompatActivity.MODE_PRIVATE).getBoolean("DM", false)
        val notification =
            requireActivity().getPreferences(AppCompatActivity.MODE_PRIVATE).getBoolean("NO", false)

        darkModeSwitch = rootView.findViewById(R.id.darkModeSwitch)
        notificationsSwitch = rootView.findViewById(R.id.notificationsSwitch)


        if (darkModeSwitch.isChecked) {
            darkModeSwitch.text = "on"
            AppCompatDelegate.setDefaultNightMode(MODE_NIGHT_YES)
        } else {
            darkModeSwitch.text = "off"
            AppCompatDelegate.setDefaultNightMode(MODE_NIGHT_NO)
        }

        if (ip != "") rootView.findViewById<EditText>(R.id.editIPText).setText(ip)
        darkModeSwitch.isChecked = darkMode
        notificationsSwitch.isChecked = notification

        darkModeSwitch.setOnClickListener {
            if (darkModeSwitch.isChecked) {
                darkModeSwitch.text = "on"
                AppCompatDelegate.setDefaultNightMode(MODE_NIGHT_YES)
            } else {
                darkModeSwitch.text = "off"
                AppCompatDelegate.setDefaultNightMode(MODE_NIGHT_NO)
            }
        }

        notificationsSwitch.setOnClickListener {
            if (notificationsSwitch.isChecked) notificationsSwitch.text = "on"
            else notificationsSwitch.text = "off"
        }


        rootView.findViewById<Button>(R.id.checkIp).setOnClickListener {
            onCheckIP()
        }

        rootView.findViewById<Button>(R.id.deleteAllDataButton).setOnClickListener {
            onDeleteAll()
        }
        rootView.findViewById<Button>(R.id.exportButton).setOnClickListener {
            if (editText.text.isEmpty() || editText.text.contains(" ") || editText.text.contains(
                    "."
                )
            ) {
                Toast.makeText(activity, "Please enter a correct filename", Toast.LENGTH_SHORT)
                    .show()
            } else {
                val data = StringBuilder()
                data.append("Entry;Date;Time;Pain;Localization;AIPain;Note;Action;Trigger;ImagePath\n")
                val savedData = EntryLoadSave.getAllEntries(requireActivity())

                for ((i, entry) in savedData?.withIndex()!!) {
                    data.append("${entry.id};")
                    data.append(entry.date + ";")
                    data.append(entry.time)
                    data.append(";")
                    data.append(entry.self_pain)
                    data.append(";")
                    if (entry.painLocalized != null) {
                        data.append(entry.painLocalized)
                    }
                    data.append(";")
                    if (entry.ai_pain != null) {
                        data.append(entry.ai_pain)
                    }
                    data.append(";")
                    if (entry.note != null && entry.note != "") {
                        data.append(entry.note)
                    }
                    data.append(";")
                    if (entry.counter_action != null && entry.counter_action != "") {
                        data.append(entry.counter_action)
                    }
                    data.append(";")
                    if (entry.trigger != null && entry.trigger != "") {
                        data.append(entry.trigger)
                    }
                    data.append(";")
                    if (entry.pathImg != null && entry.pathImg != "") {
                        data.append(entry.pathImg)
                    }
                    data.append("\n")
                }

                Log.d("logg", "Data: " + data.toString())

                try { //saving the file into device
                    val fileName = editText.text.toString()
                    val out = context?.openFileOutput("$fileName.csv", Context.MODE_PRIVATE)
                    out?.write(data.toString().toByteArray())
                    out?.close()
                    //exporting
                    val filelocation = File(requireContext().filesDir, "$fileName.csv")

                    val path = FileProvider.getUriForFile(
                        requireContext(),
                        "com.example.schmerztagebuch.fileprovider",
                        filelocation
                    )
                    val fileIntent = Intent(Intent.ACTION_SEND)
                    fileIntent.type = "text/csv"
                    fileIntent.putExtra(Intent.EXTRA_SUBJECT, "Data")
                    fileIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    fileIntent.putExtra(Intent.EXTRA_STREAM, path)
                    startActivity(Intent.createChooser(fileIntent, "Send mail"))
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }


        return rootView
    }

    private fun onCheckIP() {
        val editText = requireActivity().findViewById<EditText>(R.id.editIPText)
        if (editText.text.isEmpty() || editText.text.contains(" ") || !InetAddresses.isNumericAddress(
                editText.text.toString()
            )
        ) {
            Toast.makeText(
                activity, "${editText.text} is not a valid IP Adress", Toast.LENGTH_SHORT
            ).show()
        } else {
            if (HTTPRequestHandler.checkIP(editText.text.toString())) {
                with(
                    requireActivity().getSharedPreferences("data", AppCompatActivity.MODE_PRIVATE)
                        .edit()
                ) {
                    putString("IP", editText.text.toString())
                    apply()
                }
                Toast.makeText(
                    requireActivity(), "Now using this IP: ${editText.text}", Toast.LENGTH_SHORT
                ).show()
            } else {
                Toast.makeText(
                    activity, "Can't connect to ${editText.text}", Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    fun onDeleteAll() {
        AlertDialog.Builder(requireContext()).setTitle("Delete All Data")
            .setMessage("Are you sure you want to delete all data?\nYour Pictures will still be saved in your Gallery")
            .setPositiveButton("yes") { _, _ ->
                val settings: SharedPreferences =
                    requireActivity().getSharedPreferences("data", AppCompatActivity.MODE_PRIVATE)
                settings.edit().clear().apply()
                requireActivity().getPreferences(AppCompatActivity.MODE_PRIVATE).edit().clear()
                    .apply()
                requireActivity().finish()
            }.setNegativeButton("no") { _, _ ->

            }.create().show()
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment BlankFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) = SettingsFragment().apply {
            arguments = Bundle().apply {
                putString(ARG_PARAM1, param1)
                putString(ARG_PARAM2, param2)
            }
        }
    }
}