package com.example.schmerztagebuch

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.util.Log.d
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.schmerztagebuch.ui.settings.ImageValidation
import com.example.schmerztagebuch.ui.settings.PictureDataItem
import com.example.schmerztagebuch.ui.settings.RecyclerViewAdpater
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.concurrent.Executor

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
private const val PICK_IMAGE = 1

/**
 * A simple [Fragment] subclass.
 * Use the [SetupFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SetupFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private val REQUEST_CODE = "REQ_CODE"

    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: RecyclerViewAdpater
    private var itemList: MutableList<PictureDataItem> = mutableListOf()// Your list of data items

    var cam_uri: Uri? = null
    var img_uri: Uri? = null
    var startCamera: ActivityResultLauncher<Intent>? = null
    var askPermissions: ActivityResultLauncher<Array<String>>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_setup, container, false)

        val type = object : TypeToken<MutableList<PictureDataItem>>() {}.type

        itemList.addAll(
            Gson().fromJson(
                activity?.getSharedPreferences("data", AppCompatActivity.MODE_PRIVATE)
                    ?.getString("paths", "[]"),
                type
            )
        )
        adapter = RecyclerViewAdpater(itemList)
        recyclerView = rootView.findViewById(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = adapter

        recyclerView.setRecyclerListener {

        }

        rootView.findViewById<Button>(R.id.makeReferencePicButton).setOnClickListener {
            onMakeReferencePicture()
        }

        rootView.findViewById<Button>(R.id.onCheckReferenceButton).setOnClickListener {
            onCheckReferencePicture()
        }

        askPermissions =
            registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { result ->
                if (result.containsKey(Manifest.permission.CAMERA) && result[Manifest.permission.CAMERA]!!) {
                    pickCamera()
                } else if (result.containsKey(Manifest.permission.READ_EXTERNAL_STORAGE) && result[Manifest.permission.READ_EXTERNAL_STORAGE]!!) {
                    openGallery()
                }
            }

        startCamera = registerForActivityResult(
            ActivityResultContracts.StartActivityForResult()
        ) { result ->
            if (result.resultCode == AppCompatActivity.RESULT_OK) {
                if (cam_uri != null) {
                    val cursor = requireActivity().contentResolver.query(
                        cam_uri!!,
                        (arrayOf(MediaStore.Images.Media.DATA)),
                        null,
                        null,
                        null
                    )

                    if (cursor != null) {
                        val column_index =
                            cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
                        cursor.moveToFirst()
                        val path = cursor.getString(column_index)
                        cursor.close()

                        itemList.add(PictureDataItem(path, ImageValidation.notProved))
                        recyclerView.adapter?.notifyItemInserted(itemList.size - 1)
                    }
                } else if (img_uri != null) {
                    d("logg", "URI: $img_uri")
                    val cursor = requireActivity().contentResolver.query(
                        img_uri!!,
                        (arrayOf(MediaStore.Images.Media.DATA)),
                        null,
                        null,
                        null
                    )

                    if (cursor != null) {
                        val column_index =
                            cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
                        cursor.moveToFirst()
                        val path = cursor.getString(column_index)
                        cursor.close()

                        itemList.add(PictureDataItem(path, ImageValidation.notProved))
                        recyclerView.adapter?.notifyItemInserted(itemList.size - 1)
                    }

                }

            }
        }

        return rootView
    }

    fun saveAll() {
        with(
            requireActivity().getSharedPreferences("data", AppCompatActivity.MODE_PRIVATE).edit()
        ) {
            putString("paths", Gson().toJson(itemList))
            apply()
        }
    }

    override fun onStop() {
        super.onStop()
        saveAll()
    }

    fun onMakeReferencePicture() {
        AlertDialog.Builder(requireContext()).setTitle("Camera or Files")
            .setMessage("Take a picture or browse files?")
            .setPositiveButton("camera") { _, _ ->
                if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED
                ) {
                    askPermissions?.launch(Array<String>(1) { Manifest.permission.CAMERA })
                } else {
                    pickCamera()
                }
            }.setNegativeButton("files") { _, _ ->
                if (ContextCompat.checkSelfPermission(
                        requireContext(),
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    )
                    != PackageManager.PERMISSION_GRANTED
                ) {
                    askPermissions?.launch(Array<String>(1) { Manifest.permission.READ_EXTERNAL_STORAGE })
                } else {
                    openGallery()
                }
            }.setNeutralButton("cancel") { _, _ ->

            }.create().show()
    }

    private fun openGallery() {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "New Picture")
        values.put(MediaStore.Images.Media.DESCRIPTION, "From Files")

        img_uri = requireActivity().contentResolver.insert(
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
            values
        )
        val filesIntent = Intent().apply {
            type = "image/*"
            action = Intent.ACTION_GET_CONTENT
        }
        //img_uri?.let { requireContext().contentResolver.takePersistableUriPermission(it, Intent.FLAG_GRANT_READ_URI_PERMISSION) }

        filesIntent.putExtra(MediaStore.EXTRA_OUTPUT, img_uri)
        startCamera?.launch(filesIntent)
    }

    fun pickCamera() {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "New Picture")
        values.put(MediaStore.Images.Media.DESCRIPTION, "From Camera")

        cam_uri = requireActivity().contentResolver.insert(
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
            values
        )!!
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, cam_uri)

        startCamera?.launch(cameraIntent)
        startCamera.apply { }
    }

    fun getListOfPaths(): MutableList<String> {
        val list = mutableListOf<String>()
        for (item in itemList) {
            list.add(item.path)
        }
        return list
    }

    @SuppressLint("NotifyDataSetChanged")
    fun onCheckReferencePicture() {
        val a = AlertDialog.Builder(requireContext()).setTitle("Transmitting Data")
            .setMessage("Please wait...").setPositiveButton("ok"){_, _ ->
                d("logg", "GOOO")
            }.setOnDismissListener {
                adapter.notifyDataSetChanged()
            }.create()
        a.show()

        itemList.forEach {
            it.validation = ImageValidation.valid
        }

        HTTPRequestHandler.TransmitReferencePictures(getListOfPaths(), requireActivity()) { res ->
            for (i in res) {
                itemList[i].validation = ImageValidation.invalid
            }
            a.dismiss()
        }
        saveAll()
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment SetupFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            SetupFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

}