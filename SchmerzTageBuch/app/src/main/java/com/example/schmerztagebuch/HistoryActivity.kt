package com.example.schmerztagebuch

import HistoryAdapter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.schmerztagebuch.data.Entry
import com.example.schmerztagebuch.data.EntryLoadSave
import com.example.schmerztagebuch.data.PainLocalized
class HistoryActivity : AppCompatActivity() {
    private lateinit var historyAdapter: HistoryAdapter // Member-Variable für den Adapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)

        val recyclerView: RecyclerView = findViewById(R.id.historyRecyclerView)

        val historyList: MutableList<Entry>? = EntryLoadSave.getAllEntries(this)

        historyAdapter = historyList?.let { HistoryAdapter(it) }!!
        recyclerView.adapter = historyAdapter
        recyclerView.layoutManager = LinearLayoutManager(this)
    }

    override fun onResume() {
        super.onResume()
        // Hier kannst du den Adapter aktualisieren, wenn du zur HistoryActivity zurückkehrst
        val updatedDataList: MutableList<Entry>? = EntryLoadSave.getAllEntries(this)
        if (updatedDataList != null) {
            historyAdapter.updateData(updatedDataList)
        } // updatedDataList ist die aktualisierte Datenquelle
    }

    fun onBack(view: View) {
        finish()
    }
}
