package com.example.schmerztagebuch.data

import android.app.Activity
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson

class EntryLoadSave {

    companion object{
        private val gson = Gson()
        fun getAllEntries(context: Activity) : MutableList<Entry>?{
            val sharedPref = context.getSharedPreferences("data", Activity.MODE_PRIVATE) ?: return null

            val len = sharedPref.getInt("size", 0)
            val list = MutableList<Entry>(len) {Entry(-1)}

            for (i in 0..<len) {
                val string = sharedPref.getString("$i", "fail")
                Log.d("logg", "Looking for $i : $string")
                if(string != "fail") {
                    val entry = gson.fromJson(string, Entry::class.java)
                    list[i] = entry
                }
            }

            return list
        }
    }
}