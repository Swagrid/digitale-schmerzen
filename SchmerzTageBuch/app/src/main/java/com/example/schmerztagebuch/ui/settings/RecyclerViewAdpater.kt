package com.example.schmerztagebuch.ui.settings

import android.icu.text.SimpleDateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.net.toUri
import androidx.recyclerview.widget.RecyclerView
import com.example.schmerztagebuch.R
import com.google.android.material.floatingactionbutton.FloatingActionButton
import java.util.Locale

class RecyclerViewAdpater (private val itemList: MutableList<PictureDataItem>) : RecyclerView.Adapter<RecyclerViewAdpater.MyViewHolder>() {

    // Create a ViewHolder class to hold references to your item's views
    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val timeTextView: TextView = itemView.findViewById(R.id.recyclerViewTime)
        val dateTextView: TextView = itemView.findViewById(R.id.recyclerViewDate)
        val photoImageView: ImageView = itemView.findViewById(R.id.imageView2)
        val deleteButton: FloatingActionButton = itemView.findViewById(R.id.floatingActionButton)
        //val itemConstrainedLayout:View = itemView.findViewById(R.id.itemConstrainedLayout)

        init {
            // Fügen Sie dem Lösch-Button einen Click-Listener hinzu
            deleteButton.setOnClickListener {
                val position = adapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    // Entfernen Sie das Element aus der Listt
                    itemList.removeAt(position)

                    // Benachrichtigen Sie den Adapter über die Änderung
                    notifyItemRemoved(position)
                }
            }
        }

    }

    // Inflate the item layout and return a new ViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.recycler_view_item, parent, false)
        return MyViewHolder(itemView)
    }

    // Bind data to the views in the ViewHolder
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentItem = itemList[position]
        val sdf = SimpleDateFormat("dd.MM.YYYY", Locale.getDefault())
        val stf = SimpleDateFormat("HH:mm", Locale.getDefault())
        holder.photoImageView.setImageURI(currentItem.path.toUri())
        holder.dateTextView.text = currentItem.validation.name
        when(currentItem.validation){
            ImageValidation.invalid -> {
                holder.itemView.setBackgroundColor(ContextCompat.getColor(holder.itemView.context, R.color.button_decline))
            }
            ImageValidation.notProved -> {
                holder.dateTextView.text = "not proved"
                holder.itemView.setBackgroundColor(ContextCompat.getColor(holder.itemView.context, R.color.background_darkgrey))
            }
            ImageValidation.valid -> {
                holder.itemView.setBackgroundColor(ContextCompat.getColor(holder.itemView.context, R.color.button_accept))
            }
        }
    }

    // Return the number of items in your data source
    override fun getItemCount(): Int {
        return itemList.size
    }
}