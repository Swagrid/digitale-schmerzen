package com.example.schmerztagebuch.ui.settings

data class PictureDataItem(
    val path: String, var validation: ImageValidation
)