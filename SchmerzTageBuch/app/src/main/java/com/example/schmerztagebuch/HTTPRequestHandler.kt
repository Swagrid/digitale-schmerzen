package com.example.schmerztagebuch

import android.R.attr.data
import android.app.Activity
import android.content.Context
import android.os.Looper
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.schmerztagebuch.data.ResponseData
import com.google.gson.Gson
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File
import java.io.FileNotFoundException
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.util.concurrent.TimeUnit
import kotlin.concurrent.thread


class HTTPRequestHandler {
    companion object {
        val client = OkHttpClient().newBuilder().connectTimeout(2, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS).readTimeout(10, TimeUnit.SECONDS).build()

        val ip: String = "192.168.0.26"
        val gson = Gson()

        fun checkIP(toCheck: String): Boolean {
            val request =
                Request.Builder().url("http://${toCheck}:5000/check").build()
            var ret = false
            thread(start = true) {
                try {
                    client.newCall(request).execute().use { response ->
                        if (response.isSuccessful) ret = true
                    }
                } catch (e: java.lang.Exception) {
                    ret = false
                    e.printStackTrace()
                }
            }.join()
            return ret
        }

        fun TransmitPicture(pic: String, name: String, context: Context): Double {
            val ip = context.getSharedPreferences("data", AppCompatActivity.MODE_PRIVATE).getString(
                "IP", ip
            )
            val file = File(pic)
            val requestBody =
                MultipartBody.Builder().setType(MultipartBody.FORM).addFormDataPart("a", "b")
                    .addFormDataPart(
                        name, "$name.jpg", file.asRequestBody("image/jgp".toMediaType())
                    ).build()
            val request = Request.Builder().url("http://$ip:5000/upload").post(requestBody).build()


            var pain: Double = 0.0

            thread(start = true) {
                Looper.prepare()
                try {
                    client.newCall(request).execute().use { response ->
                        if (!response.isSuccessful) Toast.makeText(
                            context,
                            "Server dieded",
                            Toast.LENGTH_LONG
                        ).show()
                        pain = gson.fromJson(response.body?.string(), ResponseData::class.java).pain
                    }
                } catch (e: SocketTimeoutException) {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_LONG).show()
                    e.printStackTrace()
                } catch (e: FileNotFoundException) {
                    Toast.makeText(context, "Image is corrupt!", Toast.LENGTH_LONG).show()
                    e.printStackTrace()
                }
            }.join()
            return pain
        }

        fun TransmitReferencePictures(
            path_list: MutableList<String>, context: Activity, callback: (MutableList<Int>) -> Unit
        ) {
            val ip = context.getSharedPreferences("data", AppCompatActivity.MODE_PRIVATE).getString(
                "IP", ip
            )
            if (path_list.size == 0) return callback(mutableListOf())
            val reqBuilder = MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("size", path_list.size.toString())
            for (i in path_list.indices) {
                val file = File(path_list[i])
                reqBuilder.addFormDataPart(
                    "ref$i", "ref$i.jpg", file.asRequestBody("image/jgp".toMediaType())
                )
            }

            val request =
                Request.Builder().url("http://$ip:5000/reference").post(reqBuilder.build()).build()

            var ret: ResponseData

            thread(start = true) {
                Looper.prepare()
                try {
                    client.newCall(request).execute().use { response ->
                        if (!response.isSuccessful)
                            Toast.makeText(context, "Server dieded", Toast.LENGTH_LONG).show()
                        else {
                            val string = response.body?.string()
                            ret = gson.fromJson(
                                string, ResponseData::class.java
                            )
                            callback(ret.not_valid!!)
                        }
                    }
                } catch (e: SocketTimeoutException) {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_LONG).show()
                    e.printStackTrace()
                } catch (e: ConnectException) {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_LONG).show()
                    e.printStackTrace()
                } catch (e: FileNotFoundException) {
                    Toast.makeText(context, "Image is corrupt!", Toast.LENGTH_LONG).show()
                    e.printStackTrace()
                }
            }
        }
    }
}